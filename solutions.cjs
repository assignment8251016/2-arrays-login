const fs = require("fs").promises;


function findAgender(entities){
        const agenders = [];
        entities.forEach(entity => {
            if (entity["gender"] === "Agender"){
                agenders.push(entity)
            }
        });
        return agenders;
}

function ipToComponents(data){
    return data.map(entity=>entity["ip_address"].split(".").map(num=>parseInt(num)))
}


function sumSecondComponent(ips){
    return ips.reduce((sum, ip)=>sum+=ip[1],0)
}

function sumFourthComponent(ips){
    return ips.reduce((sum, ip)=>sum+=ip[3],0)
}

function fullName(data){
    return data.map(entity=>{
        const obj = new Object();
        obj[`${entity['first_name']}`+`${entity['last_name']}`] = entity;
        return obj;
    })
}

function filterDotOrgEmail(data){
    return data.filter(data=>{
        data = data["email"].split(".")
        if(data[data.length-1] === "org"){
            return data
        }
    });
}

function findAllDotOrgDotAuEmails(data){
    const domain = {au: 0, com: 0, org :0}
    data.forEach(entity=>{
        const splits = entity['email'].split(".")
        for(let index=1;index<splits.length;index++)
            if(splits[index]==='au')
                domain["au"]+=1
            else if(splits[index]==='com')
                domain["com"]+=1
            else
                domain["org"]+=1
    })
    return domain;
}

function sortDescending(data){
    return data.sort((a,b)=>{
        if(a["first_name"]<b["first_name"]){
            return 1;
        }else if(a["first_name"]>b["first_name"]){
            return -1
        }
        return 0;
    })
}

function readFile(){
    return fs.readFile("./data.json", "utf-8").then((data)=> JSON.parse(data));
}

module.exports = {
    findAgender,
    ipToComponents,
    sumSecondComponent,
    sumFourthComponent,
    fullName,
    filterDotOrgEmail,
    findAllDotOrgDotAuEmails,
    sortDescending,
    readFile
}